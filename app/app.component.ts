import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: '<controls></controls><particle-system></particle-system>'
})
export class AppComponent { }