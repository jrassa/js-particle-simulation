## In-Browser Particle System

This is my implementation of an 'In-browser particle system'.
It contains 3 simulation types: Snow, Rain v1 and Rain v2.  I could not decide between to 2 different approaches to the rain so I decided to just include both.
There are controls along the top that allow for:
* Pausing the simulation.
* Switching between simulation types.
* Adjusting several variables that effect the simulation (Note: Rain v1 does not use the 'Wind' variable)

### Try it now
[Launch Demo](http://jrassa.bitbucket.org/particle/)

### Run it yourself
    $ git clone git@bitbucket.org:jrassa/js-particle-simulation.git /path/to/repository
    $ cd /path/to/repository
    $ npm install
    $ npm start

    Open your browser and navigate to http://localhost:3000
### Notes

I decided to further challenge myself and use this as an opportunity to learn some new technologies.
The core app is implemented using [Angular 2](http://angular.io).
The drawing is implemented using [Two.js](http://two.js.org).