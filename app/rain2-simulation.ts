import { Simulation } from './simulation';

export class Rain2Simulation implements Simulation {
  name = 'Rain v2';
  playing = true;
  reset = false;
  windVelocity = 0;
  gravity = 1;
  frequency = 10;

  generateStartingVector(two: any): any {
    var x: number;
    var y: number;
    if (this.windVelocity == 0) {
      x = (Math.random() * two.width);
      y = -5;
    } else {
      var seed = (Math.random() * (two.height + two.width)) - two.height;
      if (seed < 0) {
        x = -5;
        y = seed * -1;
        x = (this.windVelocity > 0) ? -5 : (two.width + 5);
      } else {
        x = seed;
        y = -5;
      }
    }

    return {x : x, y: y};
  }

  createParticle(two: any, frameCount: number): void {
    if (frameCount % 10 < this.frequency) {
      var start = this.generateStartingVector(two);
      var circle = two.makeEllipse(start.x, start.y, 1.5, 4);
      circle.fill = '#0000FF';
      circle.scale = 1;
      circle.noStroke();
    }
  }

  isParticleDead(particle: any, width: number, height: number) {
    return (particle.translation.y > height
        || particle.translation.x > width * 1.5
        || particle.translation.x < width * -0.5);
  }

  updateParticle(particle: any) {
    particle.translation.y += this.gravity;
    particle.translation.x += this.windVelocity;
  }
}