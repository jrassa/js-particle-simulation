import { Injectable } from '@angular/core';

import { Simulation } from './simulation';
import { SnowSimulation } from './snow-simulation';
import { RainSimulation } from './rain-simulation';
import { Rain2Simulation } from './rain2-simulation';

@Injectable()
export class SimulationService {
  simulation: Simulation;
  simulations = [new SnowSimulation(), new RainSimulation(), new Rain2Simulation()];

  getSimulation(): Simulation {
    if (this.simulation == null) {
      this.simulation = this.simulations[0];
    }
    return this.simulation;
  }

  setCurrentSimulation(name: string) {
    for (var i = 0; i < this.simulations.length; i++) {
      if (this.simulations[i].name == name) {
        this.simulation = this.simulations[i];
        this.simulation.reset = true;
      }
    }
  }

  getSimulations(): Simulation[] {
    return this.simulations;
  }
}