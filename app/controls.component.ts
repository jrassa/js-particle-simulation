import { Component } from '@angular/core';

import { Simulation } from './simulation';

import { SimulationService } from './simulation.service';

@Component({
  selector: 'controls',
  template: `
    <button (click)="onPlayPauseClick()">{{playButtonLabel}}</button>
    <span>Mode</span>:
    <select (change)="onModeChange($event.target.value)">
      <option *ngFor="let simulation of simulations" value="{{simulation.name}}">{{simulation.name}}</option>
    </select>
    <span>Gravity</span>:<input (input)="onGravityChange($event)" type="range" value="0.5" min="0.5" max="2" step="0.1">    <span>Wind Speed</span>:<input (input)="onWindChange($event)" type="range" value="0" min="-2" max="2" step="0.1">
    <span>Spawn Frequency</span>:<input (input)="onFrequencyChange($event)" type="range" value="10" min="1" max="10">
  `,
  styles: [`
    :host {
      position: absolute;
      top: 0px;
      left: 0px;
      z-index: 1;
      width: 100%;
      background-color: #FFF;
      border-bottom: 1px solid gray;
    }
    select, button, input {
      margin: 5px 5px;
    }
    button {
      width: 60px;
    }
  `]
})
export class ControlsComponent {
  playButtonLabel = 'Pause';
  simulations: {};

  constructor(private simulationService: SimulationService) { }

  getSimulations(): void {
    this.simulations = this.simulationService.getSimulations();
  }

  ngOnInit(): void {
    this.getSimulations();
  }

  onPlayPauseClick(): void {
    var simulation = this.simulationService.getSimulation();
    simulation.playing = !simulation.playing;
    this.playButtonLabel = simulation.playing ? 'Pause' : 'Play';
  }

  onModeChange(name: string): void {
    this.simulationService.setCurrentSimulation(name);
  }

  onWindChange(event:any): void {
    var simulation = this.simulationService.getSimulation();
    simulation.windVelocity = Number(event.target.value);
  }

  onGravityChange(event:any): void {
    var simulation = this.simulationService.getSimulation();
    simulation.gravity = Number(event.target.value);
  }

  onFrequencyChange(event:any): void {
    var simulation = this.simulationService.getSimulation();
    simulation.frequency = Number(event.target.value);
  }
}