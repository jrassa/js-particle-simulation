import { Simulation } from './simulation';

export class SnowSimulation implements Simulation {
  name = 'Snow';
  playing = true;
  reset = false;
  windVelocity = 0;
  gravity = 0.5;
  frequency = 10;

  generateStartingVector(two: any): any {
    var x: number;
    var y: number;
    if (this.windVelocity == 0) {
      x = (Math.random() * two.width);
      y = -5;
    } else {
      var seed = (Math.random() * (two.height + two.width)) - two.height;
      if (seed < 0) {
        x = -5;
        y = seed * -1;
        x = (this.windVelocity > 0) ? -5 : (two.width + 5);
      } else {
        x = seed;
        y = -5;
      }
    }

    return {x: x, y: y};
  }

  createParticle(two: any, frameCount: number): void {
    if (frameCount % 10 < this.frequency) {
      var size = (2 * Math.random()) + 1;
      var start = this.generateStartingVector(two);
      var circle = two.makeCircle(start.x, start.y, size);
      circle.fill = '#FFFFFF';
      circle.scale = 1;
      circle.noStroke();
    }
  }

  isParticleDead(particle: any, width: number, height: number) {
    return (particle.translation.y > height
        || particle.translation.x > width * 1.5
        || particle.translation.x < width * -0.5);
  }

  updateParticle(particle: any) {
    // Calculate speed multiplier based on particle size.
    // Larger particles will move slightly faster.
    var rect = particle.getBoundingClientRect();
    var multiplier = (rect.width / 6);

    particle.translation.y += this.gravity * multiplier;
    particle.translation.x += this.windVelocity * multiplier;
  }
}