import { Simulation } from './simulation';

export class RainSimulation implements Simulation {
  name = 'Rain v1';
  playing = true;
  reset = false;
  windVelocity = 0;
  gravity = 1;
  frequency = 10;

  generateStartingVector(two: any): any {
    return {
      x: (Math.random() * two.width),
      y: -5
    };
  }

  createParticle(two: any, frameCount: number): void {
    if (frameCount % 10 < this.frequency) {
      var size = (20 * Math.random()) + 20;
      var start = this.generateStartingVector(two);
      var circle = two.makeRectangle(start.x, start.y, 1, size);
      circle.fill = 'rgba(255,255,255, .6)'; //#FFFFFF';
      circle.scale = 1;
      circle.noStroke();
    }
  }

  isParticleDead(particle: any, width: number, height: number) {
    return (particle.translation.y > height
        || particle.translation.x > width * 1.5
        || particle.translation.x < width * -0.5);
  }

  updateParticle(particle: any) {
    particle.translation.y += this.gravity;
  }
}