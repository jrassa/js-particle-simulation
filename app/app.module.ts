import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ControlsComponent } from './controls.component';
import { ParticleSystemComponent } from './particle-system.component';

import { SimulationService } from './simulation.service';

@NgModule({
  imports: [ BrowserModule ],
  declarations: [ AppComponent, ControlsComponent, ParticleSystemComponent ],
  bootstrap: [ AppComponent ],
  providers: [ SimulationService ]
})
export class AppModule { }
