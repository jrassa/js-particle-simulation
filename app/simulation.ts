export interface Simulation {
  name: string;
  playing: boolean;
  reset: boolean;
  windVelocity: number;
  gravity: number;
  frequency: number;

  createParticle(two: any, frameCount: number): void ;

  isParticleDead(particle: any, width: number, height: number): boolean;

  updateParticle(particle: any): void;
}