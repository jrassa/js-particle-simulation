/* declare Two for compatability with two.js */
declare var Two: any;

import { Component } from '@angular/core';

import { Simulation } from './simulation';

import { SimulationService } from './simulation.service';

@Component({
  selector: 'particle-system',
  template: '<div id="particle-system-stage"></div>'
})
export class ParticleSystemComponent {
  two: any;

  constructor(private simulationService: SimulationService) { }

  ngOnInit(): void {
    var ps = this;

    var elem = document.getElementById('particle-system-stage');
    this.two = new Two({
      fullscreen: true,
      autostart: false
    }).appendTo(elem);

    // Bind a function to scale and rotate the group
    // to the animation loop.
    this.two.bind('update', function(frameCount) {
      // This code is called everytime two.update() is called.
      // Effectively 60 times per second.

      var simulation = ps.simulationService.getSimulation();

      if (simulation.reset == true) {
        ps.two.clear();
        simulation.reset = false;
      }

      if (simulation.playing) {
        var toRemove = [];

        ps.two.scene.children.forEach(function(particle) {
          if (simulation.isParticleDead(particle, ps.two.width, ps.two.height)) {
            toRemove.push(particle);
          } else {
            simulation.updateParticle(particle);
          }
        });

        ps.two.remove(toRemove);

        simulation.createParticle(ps.two, frameCount);
      }

    }).play();  // Finally, start the animation loop
  }
}